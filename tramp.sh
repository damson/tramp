#!/bin/bash

#
# TRAMP: A torified Apache + PHP + MySQL pod for Podman.
#


# Message functions. ##########################################################

# Display messages with optional prefix to standard error output.
msg() {
  local m
  printf -- "${_prefix:-}%s\n" "${1}"     >&2
  for m in "${@:2}"; do echo "${m}"; done >&2
}

# Shorthand functions for specific message types.
msg_task()  { _prefix="[\e[0;34m*\e[0m] " msg "${@}"; }
msg_warn()  { _prefix="[\e[1;33m!\e[0m] " msg "${@}"; }
msg_error() { _prefix="[\e[1;31m!\e[0m] " msg "${@}"; }


# Error handling. #############################################################

# Called when exiting after an error.
die() {
  if [[ -n "${*}" ]]; then
    msg_error "${@}"
  else
    msg_error "Failed."
  fi
  exit "${_rc:-1}"
}

# Error handler.
on_err() {
  local _rc=${?}
  if ((_rc && !BASH_SUBSHELL)); then
    die
  fi
  exit "${_rc}"
}

# Interruption handler.
on_sigint() {
  local _rc=${?}
  die "Interrupted."
}

# Install our own error handlers.
trap on_err    ERR
trap on_sigint SIGINT
set +e -Euo pipefail


# Basic definitions and checks. ###############################################

# TRAMP version number.
tramp_version="0.2.2"

# Main TRAMP command (as called) and library path.
tramp_cmd="${0}"
tramp_lib="$(dirname "$(readlink -f "${BASH_SOURCE:-${0}}")")/lib"

# Check that the TRAMP library directory is available.
if [[ ! -d "${tramp_lib}" ]]; then
  die "The TRAMP library was not found in '${tramp_lib}'." \
      "Please fix your TRAMP installation (or clone it anew)."
fi

# Check that 'podman' and 'fuse-overlayfs' are available.
for cmd in podman fuse-overlayfs; do
  if ! which "${cmd}" &>/dev/null; then
    die "The '${cmd}' program is missing." \
        "Please install the corresponding package."
  fi
done


# Load configuration and default settings. ####################################

# Load local configuration, if any.
if [[ -r "config.sh" ]]; then
  source "config.sh"
fi

# Default list of PHP extensions.
# /!\ This list must be kept sorted and separated by single spaces.
_php_extensions="bcmath exif gd imagick intl mcrypt mysqli pdo_mysql zip"

# Default configuration.
: ${NAME:="tramp"}
: ${HTML_DIR:="html"}
: ${MYSQL_DIR:="mysql"}
: ${MYSQL_USER:=${NAME}}
: ${MYSQL_PASSWORD:=${NAME}}
: ${MYSQL_DATABASE:=${NAME}}
: ${HTTP_PORT:="8080"}
: ${PHP_VERSION:="7"}
: ${PHP_EXTENSIONS:=${_php_extensions}}
: ${PHP_EXT_TAG:=}

# Default those as empty for now.
# The 'if' statements below will do the rest.
: ${MYSQL_IMAGE:=}
: ${MYSQL_VERSION:=}
: ${MARIADB_VERSION:=}

# Normalize list of PHP extensions and compute corresponding tag as
# the 6 leading characters of SHA-256 digest of the extensions list,
# or simply as 'default' if the default extensions list is used.
if [[ -n "${PHP_EXTENSIONS}" ]]; then
  PHP_EXTENSIONS=$(
    for ext in ${PHP_EXTENSIONS}; do echo "${ext}"; done \
    | LC_ALL=C sort -u | tr '\n' ' ' | sed -e 's/ $//'
  )
  if [[ "${PHP_EXTENSIONS}" == "${_php_extensions}" ]]; then
    : ${PHP_EXT_TAG:="default"}
  else
    : ${PHP_EXT_TAG:=$(sha256sum <<<"${PHP_EXTENSIONS}" | head -c6)}
  fi
fi

# Prepend '-' before PHP_EXT_TAG, if not already there.
if [[ -n "${PHP_EXT_TAG}" && "${PHP_EXT_TAG::1}" != "-" ]]; then
  PHP_EXT_TAG="-${PHP_EXT_TAG}"
fi

# Parse MySQL/MariaDB version string.
if [[ -n "${MYSQL_VERSION}" && -n "${MARIADB_VERSION}" ]]; then
  die "Configuration error:" \
      "MYSQL_VERSION and MARIADB_VERSION cannot both be defined."
elif [[ -n "${MYSQL_VERSION}"   && ! "${MYSQL_VERSION}"   =~ .*:.* ]]; then
  : ${MYSQL_IMAGE:="mysql"}
elif [[ -n "${MARIADB_VERSION}" && ! "${MARIADB_VERSION}" =~ .*:.* ]]; then
  MYSQL_IMAGE="mariadb"
  MYSQL_VERSION="${MARIADB_VERSION}"
else
  : ${MYSQL_VERSION:=${MARIADB_VERSION:-mariadb:10}}
  MYSQL_IMAGE=${MYSQL_VERSION%%:*}
  MYSQL_VERSION=${MYSQL_VERSION#*:}
fi

# Check the specified database image name.
if [[ "${MYSQL_IMAGE}" != "mysql" && "${MYSQL_IMAGE}" != "mariadb" ]]; then
  die "Configuration error:" \
      "Invalid MySQL/MariaDB image name: '${MYSQL_IMAGE}'."
fi


# Default functions for TRAMP commands. #######################################

# Display command usage.
default_cmd_usage() {
  cat <<-EOF >&2
	${cmd_desc}

	Usage: ${tramp_cmd} ${cmd} [options...]

	Options:
	  -h, --help    Display this message.
	EOF
}

# Parse command line.
default_cmd_getargs() {
  local arg what
  while ((${#})); do
    arg="${1}"; shift
    case "${arg}" in
      -h|--help)
        cmd_usage
        exit
        ;;
      *)
        [[ "${arg}" =~ ^- ]] && what="option" || what="argument"
        die "Invalid ${what}: '${arg}'." \
            "Run '${tramp_cmd} help ${cmd}' for more information."
        ;;
    esac
  done
}

# Command entry point.
# By default, this is just a simple pass-through command for 'podman pod'.
default_cmd_run() {
  podman pod "${cmd}" "${NAME}"
}


# TRAMP help messages. ########################################################

# Display usage.
tramp_usage() {
  local pad
  cat <<-EOF >&2
	TRAMP: A torified Apache + PHP + MySQL pod for Podman.

	Usage: ${tramp_cmd} <command> [options...]

	Available commands:
	EOF
  # Read the command descriptions from the 'cmd_desc' assignments
  # in the command script files.
  # Command names are right-padded to '${pad}' characters.
  pad=13
  find "${tramp_lib}/commands"       \
    -maxdepth 1 -type f -name "*.sh" \
    -exec sed -e 's/^cmd_desc="\([^"]*\)"$/\1/;  te' \
              -e "s/^cmd_desc='\([^']*\)'\$/\1/; te" \
              -e 'd; :e F' \
              {} + \
  | sed -e 's,^.*/\(.*\)\.sh$,\1,' \
        -e "/^.\{${pad}\}/!{ s/\$/$(printf "%${pad}s")/"   \
        -e "                 s/^\(.\{${pad}\}\).*\$/\1/ }" \
        -e 's/^/  /; N; s/\n/ /'   \
  | LC_ALL=C sort -k1 >&2
}

# Display TRAMP version.
tramp_version() {
  msg "TRAMP version ${tramp_version}"
}


# TRAMP script entry point. ###################################################

tramp_main() {
  local cmd arg what cmd_script

  # If a command was given, process it then exit.
  if ((${#})) && [[ ! "${1}" =~ ^- ]]; then
    cmd="${1}"; shift

    # Check that the command script exists.
    cmd_script="${tramp_lib}/commands/${cmd}.sh"
    if [[ ! -r "${cmd_script}" ]]; then
      die "Invalid command: '${cmd}'." \
          "Run '${tramp_cmd} help' for a list of available commands."
    fi

    # Load the default functions for TRAMP commands.
    # The command script will override them if necessary.
    unset cmd_desc
    cmd_usage()   { default_cmd_usage   "${@}"; }
    cmd_getargs() { default_cmd_getargs "${@}"; }
    cmd_run()     { default_cmd_run     "${@}"; }

    # Load the command script.
    source "${cmd_script}"

    # Parse command line, run command, and exit
    cmd_getargs "${@}"
    cmd_run
    exit
  fi

  # Otherwise, process command-line arguments.
  while ((${#})); do
    arg="${1}"; shift
    case "${arg}" in
      -h|--help)
        tramp_usage
        exit
        ;;
      -v|--version)
        tramp_version
        exit
        ;;
      *)
        [[ "${arg}" =~ ^- ]] && what="option" || what="argument"
        die "Invalid ${what}: '${arg}'." \
            "Run '${tramp_cmd} help' for a list of available commands."
        ;;
    esac
  done

  # If we reach this point, no command was given.
  die "Missing command." \
      "Run '${tramp_cmd} help' for a list of available commands."
}

# Call the script entry point.
tramp_main "${@}"
