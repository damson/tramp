#
# Script for 'db-empty' command.
#

# Command description.
cmd_desc="Empty the database."

# Command entry point.
cmd_run() {
  podman container exec "${NAME}-db" \
    mysql -e "DROP DATABASE IF EXISTS \`${MYSQL_DATABASE}\`;" \
          -e "CREATE DATABASE         \`${MYSQL_DATABASE}\`;"
}
