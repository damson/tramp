#
# Script for 'logs' command.
#

# Command description.
cmd_desc="Display the logs for the pod. (Ctrl-C to exit)"

# Command entry point.
cmd_run() {
  podman container logs -nf "${NAME}-"{tor,web,db}
}
