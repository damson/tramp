#
# Script for 'db-export' command.
#

# Command description.
cmd_desc="Export the database to a file."

# Display command usage.
cmd_usage() {
  cat <<-EOF >&2
	${cmd_desc}

	Usage: ${tramp_cmd} ${cmd} [options...] > dump.sql

	Options:
	  -h, --help    Display this message.
	EOF
}

# Command entry point.
cmd_run() {
  podman container exec "${NAME}-db" \
    mysqldump "${MYSQL_DATABASE}"
}
