#
# Script for 'run' command.
#


# Command help. ###############################################################

# Command description.
cmd_desc="Create and start the pod."

# Display command usage.
cmd_usage() {
  cat <<-EOF >&2
	${cmd_desc}

	Usage: ${tramp_cmd} ${cmd} [options...]

	Options:
	  -f, --force   Remove pod if it already exists.
	  -h, --help    Display this message.
	EOF
}


# Parse command line. #########################################################

cmd_getargs() {
  local arg what
  force=
  while ((${#})); do
    arg="${1}"; shift
    case "${arg}" in
      -f|--force)
        force=1
        ;;
      -h|--help)
        cmd_usage
        exit
        ;;
      *)
        [[ "${arg}" =~ ^- ]] && what="option" || what="argument"
        die "Invalid ${what}: '${arg}'." \
            "Run '${tramp_cmd} help ${cmd}' for more information."
        ;;
    esac
  done
}


# Run command. ################################################################

cmd_run() {
  local pod_extra_args run_extra_args

  # Create required data directories.
  mkdir -p "${HTML_DIR}"
  mkdir -p "${MYSQL_DIR}"

  # Check if pod already exists.
  if podman pod exists "${NAME}"; then
    if ((!force)); then
      state=$(podman pod inspect -f '{{.State}}' "${NAME}")
      case "${state}" in
        Running)
          die "Pod '${NAME}' already exists and is running." \
              "Run '${tramp_cmd} ${cmd} --force' to remove it and recreate it."
          ;;
        Exited|Stopped)
          die "Pod '${NAME}' already exists and but is stopped." \
              "Run '${tramp_cmd} start' to restart it."
          ;;
        Paused)
          die "Pod '${NAME}' already exists and but is paused." \
              "Run '${tramp_cmd} unpause' to unpause it."
          ;;
        *)
          die "Pod '${NAME}' already exists." \
              "Run '${tramp_cmd} ${cmd} --force' to remove it and recreate it."
          ;;
      esac
    else
      msg_task "Removing existing pod '${NAME}'..."
      podman pod rm -f "${NAME}"
    fi
  fi

  # Extra command-line arguments for 'podman pod create'
  # and 'podman run' commands.
  pod_extra_args=()
  run_extra_args=()

  # The '--userns' option for 'podman pod create' is only available
  # in Podman v3.4.0 and later.
  if { echo "3.4.0"; podman --version | cut -d' ' -f3; } | sort -CV; then
    pod_extra_args+=( --userns "keep-id" )
  else
    run_extra_args+=( --userns "keep-id" )
  fi

  # Create pod.
  msg_task "Creating pod '${NAME}'..."
  podman pod create \
    --replace             \
    --name    "${NAME}"   \
    --publish "${HTTP_PORT}:8080" \
    "${pod_extra_args[@]}"

  # Create and run torifying HTTP proxy container.
  msg_task "Running container '${NAME}-tor'..."
  podman run \
    --detach                \
    --restart "always"      \
    --pod     "${NAME}"     \
    --name    "${NAME}-tor" \
    --user    "root"        \
    "${run_extra_args[@]}"  \
    "localhost/tramp-tor"

  # Create and run web server container.
  msg_task "Running container '${NAME}-web'..."
  podman run \
    --detach                \
    --restart "always"      \
    --pod     "${NAME}"     \
    --name    "${NAME}-web" \
    --env     "http_proxy=127.0.0.1:8118" \
    --mount   "type=bind,source=${HTML_DIR},destination=/var/www/html" \
    "${run_extra_args[@]}"  \
    "localhost/tramp-web:php-${PHP_VERSION}${PHP_EXT_TAG}"

  # Create and run database container.
  msg_task "Running container '${NAME}-db'..."
  podman run \
    --detach                \
    --restart "always"      \
    --pod     "${NAME}"     \
    --name    "${NAME}-db"  \
    --env     "MYSQL_RANDOM_ROOT_PASSWORD=1"     \
    --env     "MYSQL_USER=${MYSQL_USER}"         \
    --env     "MYSQL_PASSWORD=${MYSQL_PASSWORD}" \
    --env     "MYSQL_DATABASE=${MYSQL_DATABASE}" \
    --mount   "type=bind,source=${MYSQL_DIR},destination=/var/lib/mysql" \
    "${run_extra_args[@]}"  \
    "docker.io/library/${MYSQL_IMAGE}:${MYSQL_VERSION}"

  # Wait for the database to be created, as storing the database credentials
  # too early will prevent en entrypoint script to bootstrap the database
  # properly.
  # Timeout after 10 seconds if the database is still not created.
  msg_task "Waiting for database '${MYSQL_DATABASE}' to be created..."
  timeout=10
  while [[ ! -d "${MYSQL_DIR}/${MYSQL_DATABASE}" ]]; do
    ((timeout--)) || die "Timed out."
    sleep 1
  done
  # Wait for an extra second, just to be safe.
  sleep 1

  # Store the database credentials in the database container to avoid
  # having to pass them via command line later on.
  msg_task "Storing database credentials in container '${NAME}-db'..."
  podman exec --interactive "${NAME}-db" \
    bash -c "tee ~/.my.cnf &>/dev/null; chmod 600 ~/.my.cnf" <<-EOF
	[client]
	user = ${MYSQL_USER}
	password = ${MYSQL_PASSWORD}
	[mysql]
	database = ${MYSQL_DATABASE}
	EOF

  # Display summary.
  msg_task "All done!"
  cat <<-EOF >&2

	Access your website at the following address:
	  http://127.0.0.1:${HTTP_PORT}

	Database configuration:
	  User name:      ${MYSQL_USER}
	  Password:       ${MYSQL_PASSWORD}
	  Database name:  ${MYSQL_DATABASE}
	  Server address: 127.0.0.1

	Quick command summary:
	  Stop pod:             ${tramp_cmd} stop
	  Start pod:            ${tramp_cmd} start
	  Remove pod:           ${tramp_cmd} rm
	  Display pod status:   ${tramp_cmd} status
	  Display pod logs:     ${tramp_cmd} logs    (Ctrl-C to exit)
	  Restart Tor proxy:    ${tramp_cmd} tor-restart
	  Empty the database:   ${tramp_cmd} db-empty
	  Export the database:  ${tramp_cmd} db-export > dump.sql
	  Import the database:  ${tramp_cmd} db-import < dump.sql
	EOF
}
