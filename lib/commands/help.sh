#
# Script for 'help' command.
#


# Command help. ###############################################################

# Command description.
cmd_desc="Display help about any command."

# Display command usage.
cmd_usage() {
  cat <<-EOF >&2
	${cmd_desc}

	Usage: ${tramp_cmd} ${cmd} [command] [options...]

	Options:
	  -h, --help    Display this message.
	EOF
}


# Parse command line. #########################################################

cmd_getargs() {
  local arg what
  while ((${#})); do
    arg="${1}"; shift
    case "${arg}" in
      # If a command is given, call the TRAMP script entry point for that
      # command and with the '--help' option.
      [^-]*)
        tramp_main "${arg}" --help || exit 1
        exit
        ;;
      -h|--help)
        cmd_usage
        exit
        ;;
      *)
        [[ "${arg}" =~ ^- ]] && what="option" || what="argument"
        die "Invalid ${what}: '${arg}'." \
            "Run '${tramp_cmd} help ${cmd}' for more information."
        ;;
    esac
  done
}


# Command entry point. ########################################################

cmd_run() {
  # Call the TRAMP script entry point with '--help'.
  tramp_main --help
}
