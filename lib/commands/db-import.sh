#
# Script for 'db-import' command.
#

# Command description.
cmd_desc="Import the database from a file."

# Display command usage.
cmd_usage() {
  cat <<-EOF >&2
	${cmd_desc}

	Usage: ${tramp_cmd} ${cmd} [options...] < dump.sql

	Options:
	  -h, --help    Display this message.
	EOF
}

# Command entry point.
cmd_run() {
  podman container exec --interactive "${NAME}-db" \
    mysql
}
