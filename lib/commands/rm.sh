#
# Script for 'rm' command.
#

# Command description.
cmd_desc="Remove the pod."

# Command entry point.
cmd_run() {
  podman pod rm -f "${NAME}"
}
