#
# Script for 'version' command.
#

# Command description.
cmd_desc="Display TRAMP version."

# Command entry point.
cmd_run() {
  # Call the TRAMP script entry point with '--version'.
  tramp_main --version
}
