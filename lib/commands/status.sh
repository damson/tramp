#
# Script for 'status' command.
#

# Command description.
cmd_desc="Display the pod status."

# Command entry point.
cmd_run() {
  if podman pod exists "${NAME}"; then
    podman pod inspect -f "{{.State}}" "${NAME}"
  else
    echo "Not found"
    exit 1
  fi
}
