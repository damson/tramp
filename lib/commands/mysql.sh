#
# Script for 'mysql' command.
#

# Command description.
cmd_desc="Open the database in a MySQL command-line client."

# Command entry point.
cmd_run() {
  podman container exec --interactive --tty "${NAME}-db" \
    mysql
}
