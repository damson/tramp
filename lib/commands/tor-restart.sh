#
# Script for 'tor-restart' command.
#

# Command description.
cmd_desc="Restart the Tor proxy."

# Command entry point.
cmd_run() {
  podman container exec "${NAME}-tor" rc-service tor restart
}
