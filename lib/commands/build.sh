#
# Script for 'build' command.
#


# Command help. ###############################################################

# Command description.
cmd_desc="Build the container images."

# Display command usage.
cmd_usage() {
  cat <<-EOF >&2
	${cmd_desc}

	Usage: ${tramp_cmd} ${cmd} [options...]

	Options:
	  -f, --force   Force build, even if images already exist.
	  -h, --help    Display this message.
	EOF
}


# Parse command line. #########################################################

cmd_getargs() {
  local arg what
  force=
  while ((${#})); do
    arg="${1}"; shift
    case "${arg}" in
      -f|--force)
        force=1
        ;;
      -h|--help)
        cmd_usage
        exit
        ;;
      *)
        [[ "${arg}" =~ ^- ]] && what="option" || what="argument"
        die "Invalid ${what}: '${arg}'." \
            "Run '${tramp_cmd} help ${cmd}' for more information."
        ;;
    esac
  done
}


# Run command. ################################################################

cmd_run() {
  local tag

  # Build torifying proxy image.
  tag="tramp-tor"
  if ((!force)) && podman image exists "${tag}"; then
    msg_task "Image '${tag}' already exists. Skipping..."
  else
    msg_task "Building image '${tag}'..."
    podman build     \
      --tag "${tag}" \
      "${tramp_lib}/containers/tor"
  fi

  # Build web server image.
  tag="tramp-web:php-${PHP_VERSION}${PHP_EXT_TAG}"
  if ((!force)) && podman image exists "${tag}"; then
    msg_task "Image '${tag}' already exists. Skipping..."
  else
    msg_task "Building image '${tag}'..."
    podman build     \
      --tag "${tag}" \
      --build-arg "PHP_VERSION=${PHP_VERSION}"       \
      --build-arg "PHP_EXTENSIONS=${PHP_EXTENSIONS}" \
      "${tramp_lib}/containers/web"
  fi

  # Pull MySQL/MariaDB image.
  tag="${MYSQL_IMAGE}:${MYSQL_VERSION}"
  if ((!force)) && podman image exists "${tag}"; then
    msg_task "Image '${tag}' already exists. Skipping..."
  else
    msg_task "Building image '${tag}'..."
    podman pull \
      "docker.io/library/${tag}"
  fi

  # Prune dangling images.
  msg_task "Removing unused images..."
  podman image prune --force

  msg_task "All done!"
}
