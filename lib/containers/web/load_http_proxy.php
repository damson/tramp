<?php

if (!empty($proxy = getenv('http_proxy'))) {

  // Use proxy for HTTPS as well, if unspecified.
  if (empty(getenv('https_proxy'))) {
    putenv('https_proxy='.$proxy);
  }

  // Set proxy as default option for HTTP(S) streams.
  $default_opts = array(
    'http' => array(
      'proxy'           => str_replace('http://', 'tcp://', $proxy),
      'request_fulluri' => true,
    ),
  );
  $default = stream_context_set_default($default_opts);
  libxml_set_streams_context($default);

}
