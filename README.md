[[Version française plus bas](#tramp-un-pod-apache-php-mysql-torifié-pour-podman)]

# TRAMP: A torified Apache + PHP + MySQL pod for Podman

* [Installation](#installation)
  * [Requirements](#requirements)
  * [Setting up rootless mode](#setting-up-rootless-mode)
* [Using TRAMP](#using-tramp)
  * [Initializing a new project](#initializing-a-new-project)
  * [Configuring TRAMP for your project](#configuring-tramp-for-your-project)
  * [Building the container images](#building-the-container-images)
  * [Running the pod](#running-the-pod)
  * [TRAMP commands](#tramp-commands)
    * [Basic commands](#basic-commands)
    * [Obtaining help](#obtaining-help)
    * [Restarting Tor](#restarting-tor)
    * [Exporting/importing database dumps](#exportingimporting-database-dumps)
    * [Going further](#going-further)
  * [Populating the website](#populating-the-website)
  * [Enjoy!](#enjoy)


## Installation

### Requirements

This project requires Podman and the `fuse-overlayfs` utility.
On Debian, you can just install the following packages:
```bash
sudo apt-get install podman fuse-overlayfs
```

You can also install Git if you want to be able to clone this
repository:
```bash
sudo apt-get install git
```

### Setting up rootless mode

Once installed, if you want to run the containers in rootless mode
(i.e., as a non-root user, referred to as `user` in the following),
you need to configure sub-UIDs and sub-GIDs for this user. To do so,
run the followng command, after replacing the `user` at the end by
the actual name of your user account:
```bash
sudo usermod --add-subuids 100000-165535 --add-subgids 100000-165535 user
```
This will allocate extra UIDs and GIDs 100000 to 165535 for this user.
If they are already taken, you can change the numeric ranges in the
`usermod` command above (such as `200000-265535`, for instance).

The sub-UIDs and sub-GIDs configured on the system are listed in files
`/etc/subuid` and `/etc/subgid`, respectively.

Once sub-UIDs and sub-GIDs are configured, you will need to log out
then log back in again for the changes to be taken into account.


## Using TRAMP

### Initializing a new project

In order to use TRAMP in a new project, first create a new directory
for this project.

You then need to copy the contents of the TRAMP repository to this
directory. If Git is installed, you can just clone the TRAMP repository
by opening a terminal in your project's directory and then running the
following command:
```bash
git clone https://0xacab.org/damson/tramp.git .
```
(Don't forget the period `.` at the end of this command!)

In the following, all indicated TRAMP commands (that is, those starting
with `./tramp.sh`) should be run from a terminal in your project's
directory.

### Configuring TRAMP for your project

The TRAMP configuration for your project is stored in a file
named `config.sh` and located in the project's directory.
An example configuration is given in `config.sh.example`, which is
bundled with TRAMP. To use this file as a starting point, copy it
as `config.sh` and put it in your project's directory.

You can then edit your project's `config.sh` file with your favorite
text editor.
This file will contain TRAMP's configuration variables specific to
your project.

The file follows the Bash syntax, meaning that all lines starting
with a hash sign (`#`) are comments, and each definition of a
configuration variable is of the form `VARIABLE="value"`.

Most configuration settings have a default value, meaning that you
don't have to define them, unless you want to change these default
settings. The default values are shown in the commented out
definitions. For instance, the name of the project is controlled
by the `NAME` variable, and its default value is `tramp`.

You can change this name by uncommenting the corresponding line
```bash
#NAME="tramp"
```
and by replacing `tramp` by your own project's name. This definition
should then read something like
```bash
NAME="myproject"
```

If you need to use specific versions of PHP or of the MySQL or MariaDB
database backend for your project, you can also define the configuration
variables `PHP_VERSION` and `MYSQL_VERSION`, respectively. For instance,
to use PHP 5.6 and MySQL 5.5, you can just write the following in your
project's `config.sh` file:
```bash
PHP_VERSION="5.6"
MYSQL_VERSION="5.5"
```

If you need to run several TRAMP pods at the same time, you will also
have to specify distinct HTTP ports for the web server to listen on,
as they cannot be shared between pods. The HTTP port is defined by
the `HTTP_PORT` variable (which is set to `8080` by default).
To have the web server listen on port 8042 instead, for instance,
just write the following definition in the configuration file:
```bash
HTTP_PORT="8042"
```
(Note that only ports 1024 and above can be used in rootless mode.)

In any case, you can always modify your project's configuration later.
You'll just have to rebuild the container images, as described below,
for your changes to be taken into account.

### Building the container images

Before being able to run the TRAMP pod, you need to build the container
images required for your project. In order to do so, you need to open
a terminal in your project's directory, then run the following command:
```bash
./tramp.sh build
```

Note that it is important that you run this command from your project's
directory, as it will read its configuration from the `config.sh` file
it will find in the current directory.

The first time you execute this command, it should run for a while as it
has to pull container images from online Docker repositories, and install
a few other packages on those images.

However, once an image is built, the command will not try to build it again,
unless you run it with the `--force` (or `-f`) command-line option.

> **Note:** If you observe the following warning messages:
> ```
> WARN[0000] The cgroupv2 manager is set to systemd but there is no systemd user session available
> WARN[0000] For using systemd, you may need to login using an user session
> WARN[0000] Alternatively, you can enable lingering with: `loginctl enable-linger 1000` (possibly as root)
> WARN[0000] Falling back to --cgroup-manager=cgroupfs
> ```
> you can try running the following command (once again replacing `user` by your
> own user name) then restart your user session for the changes to be taken into
> account:
> ```bash
> sudo loginctl enable-linger user
> ```

### Running the pod

Once the container images are built, you can just create and start the
TRAMP pod by opening a terminal in your project's directory and running
the following command:
```bash
./tramp.sh run
```

If this operation is successful, the command should display `All done!`,
followed by a short summary of useful information and commands related
to your pod.

Once the pod is running, you should be able to open the URL
<http://127.0.0.1:8080/> in your favorite browser. (If you have changed the
definition of `HTTP_PORT` in `config.sh` to something other than `8080`, you
should also change the port number accordingly in this URL.)

The browser should then display a *Forbidden* page: that's normal, as the
website is still empty and has no content to display!

> **Note:** If you are using the Tor Browser, you will first need to allow
> it to access this local address directly (*i.e.*, without using the Tor
> network). In order to do so, open the URL `about:config` in a new
> tab of the Tor Browser, and click on the *Accept the Risk and
> Continue* button that pops up. Using the search bar, look for the
> `network.proxy.no_proxies_on` preference. Click on the *Edit* button
> on the right to modify its value, write `127.0.0.1` in the box, and
> press *Enter* to record the change. You can then reload the page
> at <http://127.0.0.1:8080/> to make sure that it worked.

If you reboot your computer, your pod will still be available.
However, it will be stopped: you'll have to start it again using
the `./tramp.sh start` command. (See below for more commands for
controlling your pod.)

### TRAMP commands

#### Basic commands

TRAMP supports a few commands that you can use to control your project's
pod and its containers. In particular, you can:
- stop the pod:
  ```bash
  ./tramp.sh stop
  ```
- start the pod:
  ```bash
  ./tramp.sh start
  ```
- remove the pod:
  ```bash
  ./tramp.sh rm
  ```
- display the pod status:
  ```bash
  ./tramp.sh status
  ```
- or display the container logs (once you're done with the logs, you'll have
  to press *Ctrl-C* to exit this command and return to the terminal prompt):
  ```bash
  ./tramp.sh logs
  ```

You can also obtain a list of all existing pods:
```bash
podman pod ls
```
(Note that this is a Podman command, not a TRAMP command, so it will list
non-TRAMP pods as well.)

#### Obtaining help

You can always obtain the list of all available TRAMP commands by running
```bash
./tramp.sh help
```

You can also get help about a specific TRAMP command (`build`, for instance)
by running
```bash
./tramp.sh help build
```

#### Restarting Tor

If you need to restart the Tor proxy (so as to renew the Tor circuits,
for instance), you can also run
```bash
./tramp.sh tor-restart
```

#### Exporting/importing database dumps

If at some point you want to obtain an export (a “dump”) of the current
MySQL/MariaDB database, you can run
```bash
./tramp.sh db-export > dump.sql
```
This will generate a file `dump.sql` in the current directory. You can
then use this dump to import your database on another server, for instance.

You can also keep this dump as a snapshot of the database, in order to
import it back later on if you want to restore the database to this state:
```bash
./tramp.sh db-import < dump.sql
```

Note that the `db-import` command above does not remove the existing
tables from the database before importing the database dump: if a table
exists in the database but not in the dump, it will still exist after
the database import. If you want to completely empty the database before
running the import, you can use the following command:
```bash
./tramp.sh db-empty
```

Finally, if you want to directly access the database to enter SQL
queries, you can run the following command, which will open the databse
in a command-line MySQL (or MariaDB) client:
```bash
./tramp.sh mysql
```

#### Going further

In fact, the `./tramp.sh run` command creates a pod called `myproject`
(where `myproject` is the `NAME` of your project, as configured in the
`config.sh` file), along with three containers in that pod:
- `myproject-tor`, running the Tor proxy;
- `myproject-web`, running the Apache + PHP web server;
- `myproject-db`, running the MySQL or MariaDB database.

Therefore, all pod- and container-related Podman commands are available.
You can run `podman help pod` or `podman help container` to have a list
of these commands.

### Populating the website

The web server will serve the files located in the subdirectory indicated by
the `HTML_DIR` configuration variable (which is set to `html` by default).
Therefore, you can just put HTML, PHP or CSS files, images, etc., in this
directory, and they should appear at the corresponding URL in your browser.

For instance, you can create a simple HTML file `hello.html` in the `html`
subdirectory (or in the subdirectory indicated by the `HTML_DIR` setting in
`config.sh`), with the following content:
```html
<h1>Hello, world!</h1>
```
The file should then be available in your browser at the URL
<http://127.0.0.1:8080/hello.html>.

Note that if your website also stores some files (such that user-uploaded
files on a blog, for instance), they will be stored inside the `html`
subdirectory as well.

The database files are stored in the subdirectory indicated by the `MYSQL_DIR`
configuration variable (`mysql` by default). Removing this directory will
completely empty the database, removing all its contents.

### Enjoy!

Well, that's about it. Have fun! :)

---
---

# TRAMP: Un pod Apache + PHP + MySQL torifié pour Podman

* [Installation](#installation-1)
  * [Prérequis](#prérequis)
  * [Configuration du mode *rootless*](#configuration-du-mode-rootless)
* [Utilisation de TRAMP](#utilisation-de-tramp)
  * [Initialisation d'un nouveau projet](#initialisation-dun-nouveau-projet)
  * [Configuration de TRAMP pour le projet](#configuration-de-tramp-pour-le-projet)
  * [Construction des images des conteneurs](#construction-des-images-des-conteneurs)
  * [Création et démarrage du pod](#création-et-démarrage-du-pod)
  * [Commandes TRAMP](#commandes-tramp)
    * [Commandes de base](#commandes-de-base)
    * [Obtenir de l'aide](#obtenir-de-laide)
    * [Redémarrer Tor](#redémarrer-tor)
    * [Exporter/importer des sauvegardes de la base de données](#exporterimporter-des-sauvegardes-de-la-base-de-données)
    * [Pour aller plus loin](#pour-aller-plus-loin)
  * [Mettre du contenu sur le site](#mettre-du-contenu-sur-le-site)
  * [C'est parti !](#cest-parti)


## Installation

### Prérequis

Ce projet a besoin de Podman et de l'utilitaire `fuse-overlayfs`.
Sous Debian, il suffit d'installer les paquets suivants :
```bash
sudo apt-get install podman fuse-overlayfs
```

On peut aussi installer Git si l'on veut pouvoir cloner ce dépôt :
```bash
sudo apt-get install git
```

### Configuration du mode *rootless*

Une fois Podman installé, si l'on souhaite exécuter les conteneurs
en mode *rootless* (c'est-à-dire avec un compte utilisateurice
normal, appelé `user` dans la suite, et non pas avec le compte
`root`), il faut configurer les identifiants secondaires (*sub-UIDs*
et *sub-GIDs*, en anglais) pour ce compte. Pour cela, exécuter la
commande suivante, après avoir remplacé le `user` à la fin par
le vrai nom du compte utilisateurice :
```bash
sudo usermod --add-subuids 100000-165535 --add-subgids 100000-165535 user
```
Cette commande va allouer les identifiants secondaires de 100000 à
165535 pour cet·te utilisateurice. S'ils sont déjà utilisés, on peut
changer les intervalles numériques dans la ligne de commande ci-dessus
(pour mettre `200000-265535` à la place, par exemple).

Les identifiants secondaires configurés sur le système sont listés
dans les fichiers `/etc/subuid` (pour les identifiants d'utilisateurices)
et `/etc/subgid` (pour les identifiants de groupes).

Une fois les sous-identifiants configurés, il faut redémarrer la session
utilisateurice pour que les changements soient pris en compte.


## Utilisation de TRAMP

### Initialisation d'un nouveau projet

Afin d'utiliser TRAMP pour un nouveau projet, il faut tout d'abord
créer un nouveau répertoire pour ce projet.

Il faut ensuite copier le contenu du dépôt de TRAMP dans ce répertoire.
Si Git est installé, on peut cloner ce dépôt en ouvrant un terminal
dans le répertoire du projet puis en lançant la commande suivante :
```bash
git clone https://0xacab.org/damson/tramp.git .
```
(Attention à bien laisser le point `.` à la fin de cette commande !)

Dans la suite, toutes les commandes TRAMP indiquées (c'est-à-dire celles
qui commencent par `./tramp.sh`) sont à lancer depuis un terminal ouvert
dans le répertoire du projet.

### Configuration de TRAMP pour le projet

La configuration de TRAMP pour le projet est stockée dans un fichier
`config.sh` situé dans le répertoire du projet. Un exemple de configuration
est donné dans le fichier `config.sh.example`, fourni avec TRAMP.
Afin de l'utiliser comme point de départ, faire une copie de ce fichier
sous le nom `config.sh` et la mettre dans le répertoire du projet.

On peut alors éditer ce fichier `config.sh` avec un éditeur de
texte. Ce fichier définit les variables de configuration de TRAMP
spécifiques au projet en cours.

Ce fichier suit la syntaxe Bash, ce qui signifie que toutes les
lignes qui commencent par un dièse (`#`) sont des commentaires,
et que chaque définition d'une variable de configuration est de
la forme `VARIABLE="valeur"`.

La plupart des variables de configuration ont une valeur par défaut,
ce qui fait qu'il n'est pas nécessaire de les définir, sauf si l'on
souhaite mettre une autre valeur que la valeur par défaut.
Les valeurs par défaut sont indiquées dans les définitions mises
en commentaires. Par exemple, le nom du projet est spécifié par
la variable `NAME`, et sa valeur par défaut est `tramp`.

On peut changer cela en décommentant la ligne correspondante
```bash
#NAME="tramp"
```
et en remplaçant `tramp` par le nom du projet. Cette définition
devrait alors avoir la forme suivante :
```bash
NAME="monprojet"
```

Si le projet requiert des versions spécifiques de PHP ou du gestionnaire de
base de données MySQL ou MariaDB, il est aussi possible de définir les
variables de configuration `PHP_VERSION` et `MYSQL_VERSION`, respectivement.
Par exemple, pour utiliser PHP 5.6 et MySQL 5.5, il est possible de mettre
les définitions suivantes dans le fichier `config.sh` du projet :
```bash
PHP_VERSION="5.6"
MYSQL_VERSION="5.5"
```

Si l'on veut pouvoir avoir plusieurs pods TRAMP démarrés en même temps,
il faut aussi leur spécifier des ports HTTP distincts pour le serveur web,
car un même port ne peut pas être partagé entre plusieurs pods. Le port
HTTP est défini par la variable `HTTP_PORT` (dont la valeur par défaut
est `8080`). Pour faire en sorte que le serveur web de ce pod écoute plutôt
sur le port 8042, par exemple, il suffit de rajouter la définition suivante
dans le fichier de configuration :
```bash
HTTP_PORT="8042"
```
(Attention cependant : en mode *rootless*, seuls les ports 1024 et
supérieurs peuvent être utilisés.)

Dans tous les cas, il est toujours possible de modifier plus tard les
configuration du projet. Il faudra juste reconstruire les images des
conteneurs, comme décrit ci-dessous, afin que les changements soient pris
en compte.

### Construction des images des conteneurs

Avant de pouvoir démarrer le pod TRAMP, il faut construire les images
des conteneurs nécessaires pour le projet. Pour cela, il faut ouvrir
un terminal dans le répertoire du projet, puis lancer la commande suivante :
```bash
./tramp.sh build
```

Il est important de lancer cette commande depuis le répertoire du projet,
car elle va lire sa configuration depuis le fichier `config.sh` qu'elle
trouvera dans le répertoire courant.

À la première exécution de la commande, celle-ci va prendre un peu de temps
à s'exécuter, car il lui faudra télécharger plusieurs images depuis des
dépôts Docker puis installer des paquets supplémentaires sur ces images.

Cependant, une fois que les images sont construites, la commande évitera
de les reconstuire, sauf si on lui passe l'option `--force` (ou `-f`)
sur sa ligne de commande.

> **Remarque :** Si les messages d'avertissement suivants apparaissent :
> ```
> WARN[0000] The cgroupv2 manager is set to systemd but there is no systemd user session available
> WARN[0000] For using systemd, you may need to login using an user session
> WARN[0000] Alternatively, you can enable lingering with: `loginctl enable-linger 1000` (possibly as root)
> WARN[0000] Falling back to --cgroup-manager=cgroupfs
> ```
> on peut tenter de résoudre le problème en exécutant la ligne de commande
> suivante (en remplaçant là aussi `user` par le nom d'utilisateurice du
> compte), puis en redémarrant la session utilisateurice pour prendre ces
> changements en compte :
> ```bash
> sudo loginctl enable-linger user
> ```

### Création et démarrage du pod

Une fois les images des conteneurs construites, on peut alors créer
et démarrer le pod TRAMP en ouvrant un terminal dans le répertoire
du projet et en exécutant la commande suivante :
```bash
./tramp.sh run
```

Si cette opération a fonctionné, la commande devrait afficher le message
`All done!`, suivi de quelques informations et commandes concernant ce
pod.

Une fois le pod démarré, il est normalement possible d'ouvrir l'URL
<http://127.0.0.1:8080/> dans un navigateur web. (Si l'on a changé
la définition de `HTTP_PORT` dans `config.sh` à une autre valeur que `8080`,
il faut aussi adapter cette URL et mettre le bon numéro de port à la place.)

Le navigateur devrait alors afficher une page *Forbidden*, mais c'est normal :
le site est vide, pour l'instant, et il n'y a aucun contenu à afficher !

> **Remarque :** Si l'on utilise le Navigateur Tor, il faut tout d'abord le
> configurer pour l'autoriser à accéder à cette adresse locale sans passer par
> Tor.
> Pour cela, ouvrir l'URL `about:config` dans un nouvel onglet du navigateur,
> puis cliquer sur le bouton *Accepter le risque et poursuivre* qui apparaît
> alors. Grâce à la barre de recherche, trouver la préférence appelée
> `network.proxy.no_proxies_on`. Cliquer sur le bouton *Modifier* à
> droite afin d'éditer cette préférence, puis écrire `127.0.0.1` dans la
> zone de texte, et enfin appuyer sur *Entrée* pour valider la modification.
> Recharger alors la page à l'adresse <http://127.0.0.1:8080/> pour s'assurer
> que cela fonctonne.

Le pod ainsi créé restera disponible même après un redémarrage de l'ordinateur.
Cependant, il sera arrêté : pour l'utiliser, il faudra le redémarrer à l'aide
de la commande `./tramp.sh start`. (Voir ci-dessous pour plus d'exemples
de commandes pour contrôler le pod.)

### Commandes TRAMP

#### Commandes de base

TRAMP fournit quelques commandes que l'on peut utiliser pour contrôler
le pod du projet et ses conteneurs. En particulier, il est possible :
- d'arrêter le pod :
  ```bash
  ./tramp.sh stop
  ```
- de démarrer le pod :
  ```bash
  ./tramp.sh start
  ```
- de supprimer le pod :
  ```bash
  ./tramp.sh rm
  ```
- d'afficher l'état du pod :
  ```bash
  ./tramp.sh status
  ```
- ou d'afficher les journaux d'exécution des conteneurs (il faut ensuite faire
  *Ctrl-C* pour quitter cette commande et revenir à l'invite de commandes du
  terminal) :
  ```bash
  ./tramp.sh logs
  ```

Il est aussi possible de lister tous les pods existants :
```bash
podman pod ls
```
(Attention qu'il s'agit ici d'une commande Podman, et non d'une commande
TRAMP : elle affichera aussi les pods non gérés par TRAMP.)

#### Obtenir de l'aide

Il est toujours possible d'obtenir la liste des commandes TRAMP en lançant
la commande
```bash
./tramp.sh help
```

Il est aussi possible d'obtenir de l'aide à propos d'une commande TRAMP
particulière (`build`, par exemple) en lançant
```bash
./tramp.sh help build
```

#### Redémarrer Tor

Si nécessaire, il est possible de redémarrer le proxy Tor (afin de renouveler
les circuits Tor, par exemple) grâce à la commande suivante :
```bash
./tramp.sh tor-restart
```

#### Exporter/importer des sauvegardes de la base de données

Si l'on souhaite à un moment faire un export (un « dump ») de la base de
données, on peut lancer la commande suivante :
```bash
./tramp.sh db-export > dump.sql
```
Celle-ci va produire un fichier `dump.sql` dans le répertoire courant.
Il est ensuite possible d'importer ce fichier sur un autre serveur de base
de données, par exemple.

Il est aussi possible de conserver cette sauvegarde de la base de données
afin de pouvoir l'importer plus tard pour restaurer la base de données
dans l'état sauvegardé :
```bash
./tramp.sh db-import < dump.sql
```

Il est à noter que la commande `db-import` ci-dessus ne supprime pas
les tables existantes de la base de données avant d'importer le dump :
si une table existe dans la base de données mais pas dans le dump, elle
existera toujours après l'import. Afin de vider complètement la base de
données avant de faire un import, on peut utiliser la commande suivante :
```bash
./tramp.sh db-empty
```

Enfin, afin d'accéder directement à base de données pour exécuter des
requêtes SQL, il est possible d'ouvrir la base de donnée dans un client
MySQL (ou MariaDB) en ligne de commande grâce à la commande
```bash
./tramp.sh mysql
```

#### Pour aller plus loin

En fait, la commande `./tramp.sh run` crée un pod appelé `monprojet`
(où `monprojet` est le nom donné au projet par la variable de configuration
`NAME` dans le fichier `config.sh`), ainsi que trois conteneurs dans ce pod :
- `monprojet-tor`, qui contient le proxy Tor ;
- `monprojet-web`, qui contient le serveur web Apache + PHP web ;
- `monprojet-db`, qui contient le serveur de base de données
  (MySQL ou MariaDB).

Par conséquent, toutes les commandes Podman liées aux pods et aux conteneurs
sont disponibles. On peut obtenir la liste de ces commandes en faisant
`podman help pod` ou `podman help container`, respectivement.

### Mettre du contenu sur le site

Le serveur web affiche les fichiers situés dans le sous-répertoire spécifié
par la variable de configuration `HTML_DIR` (dont la valeur par défaut est
`html`). Ainsi, on peut directement mettre des fichiers HTML, PHP, CSS,
des images, etc., dans ce répertoire, et elles devraient apparaître dans
le navigateur à l'adresse correspondante.

Par exemple, on peut créer un petit fichier HTML appelé `hello.html` dans
le sous-répertoire `html` (ou dans le sous-répertoire indiqué par la variable
`HTML_DIR` dans le fichier `config.sh`), avec le contenu suivant :
```html
<h1>Hello, world!</h1>
```
Ce fichier devrait alors être accessible dans le navigateur depuis l'adresse
<http://127.0.0.1:8080/hello.html>.

Si le site web stocke aussi des fichiers (par exemple, sur un blog, des
fichiers téléversés par des utilisateurices), ces fichiers seront aussi
stockés dans le sous-répertoire `html`.

Les fichiers de la base de données, quant à eux, sont stockés dans le
sous-répertoire spécifié par la variable de configuration `MYSQL_DIR`
(`mysql` par défaut). Supprimer ce répertoire permet de vider complètement
la base de données, en supprimant tout son contenu.

### C'est parti !

Voilà, c'est à peu près tout. Amusez-vous bien ! :)
